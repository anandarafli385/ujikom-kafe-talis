-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 25, 2022 at 03:50 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ukk_kasir`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id_transaksi` varchar(255) NOT NULL,
  `id_user` varchar(255) NOT NULL,
  `id_menu` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `jumlah` int(100) NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `waktu_transaksi` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id_transaksi`, `id_user`, `id_menu`, `qty`, `jumlah`, `note`, `waktu_transaksi`) VALUES
('1475206', 'M001', 'ETM', 5, 17500, '', '2022-03-24 04:15:48'),
('1475206', 'M001', 'ETM', 5, 17500, '', '2022-03-24 04:15:48'),
('1475206', 'M001', 'ETM', 5, 17500, '', '2022-03-24 04:15:48'),
('1475206', 'M001', 'AB', 5, 100000, '', '2022-03-24 04:15:48'),
('1475206', 'M001', 'ETM', 5, 17500, '', '2022-03-24 04:15:48'),
('1475206', 'M001', 'ETM', 5, 17500, '', '2022-03-24 04:15:48'),
('1475206', 'M001', 'ETM', 5, 17500, '', '2022-03-24 04:15:48'),
('1475206', 'M001', 'AB', 5, 100000, '', '2022-03-24 04:15:48'),
('2529037', 'M001', 'AB', 5, 100000, '', '2022-03-24 04:50:48'),
('2529037', 'M001', 'AG', 5, 95000, '', '2022-03-24 04:50:48'),
('2529037', 'M001', 'JA', 5, 45000, '', '2022-03-24 04:50:48'),
('2335421', 'M001', 'NB', 5, 60000, '', '2022-03-25 14:48:52'),
('2335421', 'M001', 'NB', 5, 60000, '', '2022-03-25 14:48:52'),
('3540733', 'M001', 'JA', 5, 45000, '', '2022-03-25 14:49:16'),
('3540733', 'M001', 'JA', 5, 45000, '', '2022-03-25 14:49:16'),
('3540733', 'M001', 'JA', 5, 45000, '', '2022-03-25 14:49:16');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` varchar(255) NOT NULL,
  `nama_menu` text NOT NULL,
  `harga` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `nama_menu`, `harga`) VALUES
('AB', 'Ayam Bakar', '20000'),
('AG', 'Ayam Goreng', '19000'),
('ETM', 'Es Teh Manis', '3500'),
('JA', 'Jus Alpukat', '9000'),
('NB', 'Nasi Bakar', '12000'),
('NK', 'Nasi Kebuli', '15000'),
('NU', 'Nasi Uduk', '8000'),
('SA', 'Soto Ayam', '23000');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` varchar(255) NOT NULL,
  `username` text NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `level`) VALUES
('A001', 'Admin', '$2y$10$eRlxakOYpQZVIYE6JLS4heuyGFCxYOu/jfYmTsRKBova8.WGNj.ty', 2),
('K001', 'Kasir', '$2y$10$hHN.ux5A4JOJtdBgaLVeu.0y11WVd.54emduPbJ61pPAM8xbIpGdq', 3),
('M001', 'Manajer', '$2y$10$CdT4lxuMalcefXVayoQ8L.KxVI0TxUpSUYDazEFD71wdk4uyJYiB2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE `user_log` (
  `id_log` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `log_type` int(11) NOT NULL,
  `log_desc` varchar(255) NOT NULL,
  `last_activity` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id_log`, `username`, `log_type`, `log_desc`, `last_activity`) VALUES
(1, 'Manajer', 2, 'menambahkan data menu', '2022-02-26 07:48:41'),
(2, 'Manajer', 2, 'menambahkan data menu', '2022-02-26 07:50:06'),
(3, 'Manajer', 4, 'menghapus data menu', '2022-02-26 07:50:10'),
(4, 'Manajer', 4, 'menghapus data menu', '2022-02-26 07:50:32'),
(5, 'Manajer', 1, 'user logout', '2022-02-26 07:59:32'),
(6, 'Manajer', 0, 'user login', '2022-02-26 08:01:08'),
(7, 'Kasir', 0, 'user login', '2022-02-26 23:09:45'),
(8, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 05:17:57'),
(9, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 05:18:09'),
(10, 'Kasir', 4, 'menghapus data transaksi', '2022-02-27 05:18:39'),
(11, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 05:26:31'),
(12, 'Kasir', 0, 'user login', '2022-02-27 10:56:20'),
(13, 'Kasir', 1, 'user logout', '2022-02-27 13:38:40'),
(14, 'Kasir', 0, 'user login', '2022-02-27 13:42:44'),
(15, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 14:06:43'),
(16, 'Kasir', 4, 'menghapus data transaksi', '2022-02-27 14:21:12'),
(17, 'Kasir', 4, 'menghapus data transaksi', '2022-02-27 14:21:14'),
(18, 'Kasir', 4, 'menghapus data transaksi', '2022-02-27 14:21:16'),
(19, 'Kasir', 4, 'menghapus data transaksi', '2022-02-27 14:21:17'),
(20, 'Kasir', 4, 'menghapus data transaksi', '2022-02-27 14:21:18'),
(21, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 14:22:35'),
(22, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 14:22:55'),
(23, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 14:23:04'),
(24, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 14:23:11'),
(25, 'Kasir', 2, 'menambahkan data menu', '2022-02-27 14:23:49'),
(26, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 14:24:10'),
(27, 'Kasir', 2, 'menambahkan data menu', '2022-02-27 14:33:43'),
(28, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 14:34:02'),
(29, 'Kasir', 2, 'menambahkan data menu', '2022-02-27 14:52:07'),
(30, 'Kasir', 3, 'mengubah data menu', '2022-02-27 14:52:29'),
(31, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 14:52:43'),
(32, 'Kasir', 2, 'menambahkan data menu', '2022-02-27 14:54:26'),
(33, 'Kasir', 2, 'menambahkan data transaksi', '2022-02-27 14:55:03'),
(34, 'Kasir', 1, 'user logout', '2022-02-27 15:18:13'),
(35, 'Manajer', 0, 'user login', '2022-02-27 15:18:17'),
(36, 'Manajer', 1, 'user logout', '2022-02-27 15:24:58'),
(37, 'Kasir', 0, 'user login', '2022-02-27 15:25:04'),
(38, 'Kasir', 1, 'user logout', '2022-02-27 15:25:23'),
(39, 'Admin', 0, 'user login', '2022-02-27 15:25:29'),
(40, 'Admin', 1, 'user logout', '2022-02-27 15:26:57'),
(41, 'Manajer', 0, 'user login', '2022-02-27 15:27:01'),
(42, 'Manajer', 1, 'user logout', '2022-02-27 15:27:26'),
(43, 'Admin', 0, 'user login', '2022-02-27 15:27:30'),
(44, 'Admin', 1, 'user logout', '2022-02-27 15:32:04'),
(45, 'Kasir', 0, 'user login', '2022-02-27 15:45:48'),
(46, 'Kasir', 1, 'user logout', '2022-02-27 15:51:00'),
(47, 'Manajer', 0, 'user login', '2022-02-27 15:52:42'),
(48, 'Manajer', 1, 'user logout', '2022-02-27 15:56:19'),
(49, 'Manajer', 1, 'user logout', '2022-02-27 15:59:05'),
(50, 'Manajer', 0, 'user login', '2022-02-27 16:03:41'),
(51, 'Manajer', 1, 'user logout', '2022-02-27 16:04:15'),
(52, 'Kasir', 0, 'user login', '2022-02-27 19:50:00'),
(53, 'Kasir', 0, 'user login', '2022-02-27 19:56:32'),
(54, 'Kasir', 1, 'user logout', '2022-02-27 19:58:48'),
(55, 'Manajer', 0, 'user login', '2022-02-28 04:12:15'),
(56, 'Manajer', 2, 'menambahkan data menu', '2022-02-28 04:26:04'),
(57, 'Manajer', 3, 'mengubah data menu', '2022-02-28 04:28:16'),
(58, 'Manajer', 3, 'mengubah data menu', '2022-02-28 04:28:27'),
(59, 'Manajer', 2, 'menambahkan data user', '2022-02-28 04:33:43'),
(60, 'Manajer', 3, 'mengubah data user', '2022-02-28 04:41:10'),
(61, 'Manajer', 3, 'mengubah data user', '2022-02-28 04:41:17'),
(62, 'Manajer', 3, 'mengubah data menu', '2022-02-28 04:43:01'),
(63, 'Manajer', 2, 'menambahkan data transaksi', '2022-02-28 04:50:41'),
(64, 'Manajer', 3, 'mengubah data transaksi', '2022-02-28 05:58:03'),
(65, 'Manajer', 3, 'mengubah data transaksi', '2022-02-28 05:58:15'),
(66, 'Manajer', 4, 'menghapus data user', '2022-02-28 06:14:43'),
(67, 'Manajer', 1, 'user logout', '2022-02-28 06:14:48'),
(68, 'Manajer', 0, 'user login', '2022-02-28 06:15:02'),
(69, 'Manajer', 3, 'mengubah data user', '2022-02-28 06:15:24'),
(70, 'Manajer', 1, 'user logout', '2022-02-28 06:15:40'),
(71, 'Kasir', 0, 'user login', '2022-02-28 06:15:43'),
(72, 'Manajer', 0, 'user login', '2022-03-01 00:31:34'),
(73, 'Manajer', 1, 'user logout', '2022-03-01 01:35:13'),
(74, 'Manajer', 0, 'user login', '2022-03-01 01:39:13'),
(75, 'Manajer', 0, 'user login', '2022-03-01 01:41:42'),
(76, 'Manajer', 3, 'mengubah data user', '2022-03-01 01:49:33'),
(77, 'Manajer', 3, 'mengubah data user', '2022-03-01 01:49:46'),
(78, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-01 01:52:10'),
(79, 'Manajer', 1, 'user logout', '2022-03-01 03:01:49'),
(80, 'Manajer', 0, 'user login', '2022-03-01 03:01:55'),
(81, 'Manajer', 0, 'user login', '2022-03-01 05:44:39'),
(82, 'Manajer', 3, 'mengubah data user', '2022-03-01 06:02:16'),
(83, 'Manajer', 3, 'mengubah data user', '2022-03-01 06:03:02'),
(84, 'Manajer', 3, 'mengubah data user', '2022-03-01 06:04:00'),
(85, 'Manajer', 4, 'menghapus data transaksi', '2022-03-01 06:05:16'),
(86, 'Manajer', 4, 'menghapus data transaksi', '2022-03-01 06:05:19'),
(87, 'Manajer', 3, 'mengubah data user', '2022-03-01 06:06:03'),
(88, 'Manajer', 0, 'user login', '2022-03-01 06:06:38'),
(89, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-01 06:15:34'),
(90, 'Manajer', 3, 'mengubah data menu', '2022-03-01 06:16:24'),
(91, 'Manajer', 3, 'mengubah data menu', '2022-03-01 06:17:06'),
(92, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-01 06:18:09'),
(93, 'Manajer', 0, 'user login', '2022-03-01 09:00:44'),
(94, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-01 10:21:16'),
(95, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-01 12:00:55'),
(96, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-01 12:01:10'),
(97, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-01 12:01:28'),
(98, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-01 12:01:41'),
(99, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-01 12:35:50'),
(100, 'Manajer', 0, 'user login', '2022-03-02 00:59:07'),
(101, 'Manajer', 0, 'user login', '2022-03-02 14:01:09'),
(102, 'Manajer', 0, 'user login', '2022-03-02 14:01:33'),
(103, 'Manajer', 0, 'user login', '2022-03-03 00:01:27'),
(104, 'Manajer', 0, 'user login', '2022-03-03 04:48:57'),
(105, 'Manajer', 0, 'user login', '2022-03-03 12:28:34'),
(106, 'Manajer', 0, 'user login', '2022-03-04 00:06:57'),
(107, 'Manajer', 1, 'user logout', '2022-03-04 00:10:03'),
(108, 'Manajer', 0, 'user login', '2022-03-04 00:10:27'),
(109, 'Manajer', 0, 'user login', '2022-03-04 02:36:19'),
(110, 'Manajer', 0, 'user login', '2022-03-04 05:55:25'),
(111, 'Manajer', 0, 'user login', '2022-03-04 09:33:25'),
(112, 'Manajer', 0, 'user login', '2022-03-05 06:40:45'),
(113, 'Manajer', 0, 'user login', '2022-03-06 06:03:11'),
(114, 'Manajer', 0, 'user login', '2022-03-06 09:27:59'),
(115, 'Manajer', 0, 'user login', '2022-03-18 00:18:15'),
(116, 'Manajer', 1, 'user logout', '2022-03-18 00:18:33'),
(117, 'Admin', 0, 'user login', '2022-03-18 00:20:27'),
(118, 'Admin', 3, 'mengubah data user', '2022-03-18 00:20:37'),
(119, 'Admin', 1, 'user logout', '2022-03-18 00:20:41'),
(120, 'Kasir', 0, 'user login', '2022-03-18 00:20:47'),
(121, 'Kasir', 0, 'user login', '2022-03-20 14:06:33'),
(122, 'Manajer', 0, 'user login', '2022-03-20 14:13:46'),
(123, 'Manajer', 1, 'user logout', '2022-03-20 14:13:49'),
(124, 'Admin', 0, 'user login', '2022-03-20 14:13:55'),
(125, 'Admin', 1, 'user logout', '2022-03-20 14:13:57'),
(126, 'Kasir', 0, 'user login', '2022-03-20 14:14:04'),
(127, 'Manajer', 0, 'user login', '2022-03-20 14:15:34'),
(128, 'Manajer', 1, 'user logout', '2022-03-20 14:19:57'),
(129, 'Manajer', 0, 'user login', '2022-03-20 14:20:19'),
(130, 'Manajer', 4, 'menghapus data user', '2022-03-20 14:20:38'),
(131, 'Manajer', 2, 'menambahkan data user', '2022-03-20 14:20:49'),
(132, 'Manajer', 1, 'user logout', '2022-03-20 14:20:53'),
(133, 'Kasir', 0, 'user login', '2022-03-20 14:20:58'),
(134, 'Manajer', 0, 'user login', '2022-03-20 14:21:14'),
(135, 'Manajer', 2, 'menambahkan data user', '2022-03-20 14:21:45'),
(136, 'Manajer', 1, 'user logout', '2022-03-20 14:21:48'),
(137, 'Kasirs', 0, 'user login', '2022-03-20 14:21:53'),
(138, 'Kasirs', 1, 'user logout', '2022-03-20 14:21:57'),
(139, 'Manajer', 0, 'user login', '2022-03-20 14:22:02'),
(140, 'Manajer', 4, 'menghapus data user', '2022-03-20 14:22:12'),
(141, 'Manajer', 1, 'user logout', '2022-03-20 14:22:16'),
(142, 'Kasir', 0, 'user login', '2022-03-20 14:22:20'),
(143, 'Manajer', 0, 'user login', '2022-03-20 14:28:33'),
(144, 'Manajer', 1, 'user logout', '2022-03-20 14:28:39'),
(145, 'Kasir', 0, 'user login', '2022-03-20 14:28:42'),
(146, 'Kasir', 0, 'user login', '2022-03-20 14:31:07'),
(147, 'Manajer', 0, 'user login', '2022-03-20 15:07:48'),
(148, 'Manajer', 1, 'user logout', '2022-03-20 15:07:57'),
(149, 'Admin', 0, 'user login', '2022-03-20 15:08:20'),
(150, 'Admin', 1, 'user logout', '2022-03-20 15:08:24'),
(151, 'Kasir', 0, 'user login', '2022-03-20 15:08:32'),
(152, 'Manajer', 0, 'user login', '2022-03-20 15:08:52'),
(153, 'Kasir', 0, 'user login', '2022-03-22 19:33:54'),
(154, 'Kasir', 1, 'user logout', '2022-03-22 19:53:02'),
(155, 'Manajer', 0, 'user login', '2022-03-22 19:56:02'),
(156, 'Manajer', 0, 'user login', '2022-03-23 05:45:57'),
(157, 'Manajer', 0, 'user login', '2022-03-23 10:50:00'),
(158, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 13:06:57'),
(159, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:19:20'),
(160, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:19:20'),
(161, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:22:50'),
(162, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:22:50'),
(163, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:22:55'),
(164, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:22:55'),
(165, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:24:40'),
(166, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:24:40'),
(167, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:24:41'),
(168, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:24:41'),
(169, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:24:42'),
(170, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:24:42'),
(171, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:24:43'),
(172, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:24:43'),
(173, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:25:02'),
(174, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:25:02'),
(175, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:25:02'),
(176, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 14:25:02'),
(177, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:06:41'),
(178, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:06:41'),
(179, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:06:41'),
(180, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:06:41'),
(181, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:06:41'),
(182, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:06:44'),
(183, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:06:44'),
(184, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:06:44'),
(185, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:06:44'),
(186, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:06:45'),
(187, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:10:24'),
(188, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:10:24'),
(189, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:10:24'),
(190, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:10:24'),
(191, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:13:10'),
(192, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:13:10'),
(193, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:13:10'),
(194, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:13:11'),
(195, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:13:16'),
(196, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:13:16'),
(197, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:13:16'),
(198, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:13:16'),
(199, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:13:59'),
(200, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:13:59'),
(201, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:14:00'),
(202, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:14:00'),
(203, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:22:42'),
(204, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:22:42'),
(205, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:22:42'),
(206, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:22:43'),
(207, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:23:43'),
(208, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:23:43'),
(209, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:23:43'),
(210, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:23:43'),
(211, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:34:51'),
(212, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-23 15:34:51'),
(213, 'Manajer', 0, 'user login', '2022-03-24 02:15:18'),
(214, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:03:16'),
(215, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:03:16'),
(216, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:03:16'),
(217, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:04:31'),
(218, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:04:32'),
(219, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:04:32'),
(220, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:04:35'),
(221, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:04:35'),
(222, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:04:35'),
(223, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:04:51'),
(224, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:04:51'),
(225, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:37:40'),
(226, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:37:41'),
(227, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:37:41'),
(228, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 03:37:41'),
(229, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:24:09'),
(230, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:24:09'),
(231, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:24:09'),
(232, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:24:10'),
(233, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:24:11'),
(234, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:24:12'),
(235, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:24:12'),
(236, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:24:12'),
(237, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:51:00'),
(238, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:51:00'),
(239, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-24 04:51:00'),
(240, 'Manajer', 1, 'user logout', '2022-03-24 08:43:36'),
(241, 'Manajer', 0, 'user login', '2022-03-24 08:43:44'),
(242, 'Manajer', 1, 'user logout', '2022-03-24 09:48:31'),
(243, 'Manajer', 0, 'user login', '2022-03-24 17:08:03'),
(244, 'Manajer', 0, 'user login', '2022-03-25 14:48:37'),
(245, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-25 14:49:00'),
(246, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-25 14:49:01'),
(247, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-25 14:49:30'),
(248, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-25 14:49:30'),
(249, 'Manajer', 2, 'menambahkan data transaksi', '2022-03-25 14:49:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_menu` (`id_menu`) USING BTREE;

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id_log`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `detail_transaksi_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`),
  ADD CONSTRAINT `detail_transaksi_ibfk_3` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Transaksi extends CI_Model 
{
	public function AddTransaksi($trx,$usr,$mnu,$qt,$jml,$nte,$wkt)
	{
		$data = [
			"id_transaksi" => $trx,
			"id_user" => $usr,
			"id_menu" => $mnu,
			"qty" => $qt,
			"jumlah" => $jml,
			"note" => $nte,
			"waktu_transaksi" => $wkt
		];

		$this->db->insert('detail_transaksi', $data);
	}


	public function DeleteTransaksi($id_transaksi)
	{
        $this->db->delete('detail_transaksi', array('id_transaksi' => $id_transaksi));
	}

	public function getAllTransaksi()
	{
		return $this->db->get('detail_transaksi')->result_array();
	}

	public function getIdTransaksi($id_transaksi)
	{
		return $this->db->get_where('detail_transaksi',['id_transaksi'=> $id_transaksi])->row_array();
	}

	public function getByIdTransaksi($id_transaksi)
	{
		$this->db->select('*');
		return $this->db->get_where('detail_transaksi',['id_transaksi'=> $id_transaksi])->result_array();
	}

   public function joinData(){
      $this->db->select(
      	'detail_transaksi.*,
      	 menu.id_menu,
      	 menu.nama_menu,
      	 menu.harga,
      	 user.id_user,
      	 user.username,
      	');
      $this->db->from('detail_transaksi');
      $this->db->join('user','user.id_user = detail_transaksi.id_user');      
      $this->db->join('menu','menu.id_menu = detail_transaksi.id_menu');
      $this->db->order_by('id_transaksi', 'DESC');      
	  return $this->db->get()->result_array();
   }

	public function countTransaksi()
	{
		return $this->db->from('menu')->count_all_results();
	}

	public function totalQuantity()
	{
		$this->db->select_sum('qty');
		$this->db->from('detail_transaksi');
		return $this->db->get()->row()->qty;
	}

	public function getQuantity()
	{
		return $this->db->query("SELECT SUM(`qty`) FROM `detail_transaksi` GROUP BY `id_menu` DESC")->result_array();
	}

	public function getMenuTransc()
	{
		$this->db->select(
      	'detail_transaksi.*,
      	 menu.id_menu,
      	 menu.nama_menu,
      	 menu.harga,
      	 user.id_user,

      	');
      $this->db->from('detail_transaksi');
      $this->db->join('menu','menu.id_menu = detail_transaksi.id_menu');
      $this->db->join('user','detail_transaksi.id_user = user.id_user');
      $this->db->order_by('detail_transaksi.id_menu', 'DESC');      
	  	$array = $this->db->get()->result_array();
	  	$arr = array_column($array,"nama_menu");
	  	return($arr);
	}
}

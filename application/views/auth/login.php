<!DOCTYPE html>
<html lang="en">
  <head>
  	<title><?= $title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="<?=base_url()?>assets/vendors/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="<?=base_url()?>assets/vendors/login/css/style.css">

	</head>
	<body class="img js-fullheight" style="background-image: url(<?=base_url()?>assets/vendors/login/images/bg.jpg);">
	<section class="ftco-section">
		<div class="container card bg-transparent">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Welcome To Cafe Talis</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-4">
				  <div class="card rounded-lg" style="background-color: #8a8a8a !important;">
				    <div class="login-wrap p-0 mx-3 my-3">
		      		<h2 class="mb-4 text-center heading-section">Cafe Talis Management</h2>
			        <?php if ($this->session->flashdata('auth_msg')) {
			            echo $this->session->flashdata('auth_msg');
			        }
			        $this->session->unset_userdata('auth_msg');
			        ?>
		      		<?= form_open(''); ?>
					      	<div class="form-group">
					      		<input type="text" name="username" class="form-control <?= form_error('username') ? 'invalid' : '' ?>" value="<?= set_value('username') ?>" placeholder="Username" required>
					      	</div>
			            <div class="form-group">
			              	<input id="password-field" name="password" type="password" class="form-control <?= form_error('password') ? 'invalid' : '' ?>" value="<?= set_value('password') ?>" placeholder="Password" required>
			              	<span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
			            </div>
			            <div class="form-group">
			            	<button href="<?=base_url('Dashboard')?>" class="form-control btn btn-primary submit px-3" type="submit" name="login" value="Login">Login</button>
			            </div>
		          	</form>
				  	</div>
			    </div>
				</div>
			</div>
		</div>
	</section>

  <script src="<?=base_url()?>assets/vendors/login/js/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/vendors/login/js/popper.js"></script>
  <script src="<?=base_url()?>assets/vendors/login/js/bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/vendors/login/js/main.js"></script>
</body>
</html>

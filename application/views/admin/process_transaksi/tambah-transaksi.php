        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header mt-5 pt-2">
              <h3 class="page-title"> Form Tambah Transaksi </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Data Transaksi</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Tambah Transaksi</li>
                </ol>
              </nav>
            </div>
            <div>
            </div>
            <div class="row">
              <div class="col-4 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <?= form_open(); ?>
                    <div class="form-group">
                      <label for="waktu_transaksi">Waktu Transaksi</label>
                      <input type="text" name="waktu_transaksi" id="waktu_transaksi" class="form-control text-dark" value="<?php $tz = 'Asia/Jakarta';
                                   $dt = new DateTime("now", new DateTimeZone($tz));
                                   $timestamp = $dt->format('Y-m-d G:i:s');
                                   echo $timestamp;
                      ?>" disabled>
                    </div>
                    <div class="form-group">
                      <label for="id_user">Kasir</label>
                      <input type="text" name="id_user" id="id_user" class="form-control text-dark" value="<?= $this->session->userdata('id_user') ?>" disabled>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-4 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <?= form_open(); ?>
                    <div class="form-group row">
                      <div class="col-md-12">
                      <label for="id_menu">Nama Menu</label>
                          <select class="form-control text-white" id="menu_transaksi" name="id_menu" data-live-search="true" data-min-options="1" required>
                              <option disabled selected>Pilih Menu</option>
                            <?php foreach ($menu as $mn) : ?>
                              <option value='<?= $mn['harga'] ?>' id='<?= $mn['id_menu'] ?>' name='<?= $mn['id_menu'] ?>'><?= $mn['nama_menu'] ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="qty">Quantity</label>
                      <input type="number" name="qty" id="qty_transaksi" class="form-control text-white">
                    </div>
                    <button type="button" name="tambah-item" id="btn-trx" onclick="totalBtn()" class="btn btn-primary mr-2">Tambah Menu</button>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-4 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h1 class="display-3">Invoice#<?= $invoice ?></h1>
                    <div class="form-group mt-4 pt-2">
                      <label for="qty">Sub Total</label>
                      <input type="number" name="subtotal" value="0" id="total_transaksi" class="form-control text-dark" disabled>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <h4 class="card-title">Data Transaksi</h4>
                        <p class="card-description"> Tabel ini berisi data transaksi Cafe Talis</p>
                      </div>
                    </div>
                    <div class="table-responsive">
                      <table id="add-transaksi" class="table table-hover text-white py-4">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>Id Transaksi</th>
                            <th>Id User</th>
                            <th>Nama Menu</th>
                            <th>Quantity</th>
                            <th>Jumlah Pemesanan</th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td class="d-none" id="nom"></td>
                              <td class="d-none" id="id_trx"><?= $invoice ?></td>
                              <td class="d-none" id="usr_tx"><?= $this->session->userdata('id_user') ?></td>
                              <td class="d-none" id="menu_trx"></td>
                              <td class="d-none" id="qty_trx"></td>
                              <td class="d-none" id="jml_trx"></td>
                            </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <?= form_open(); ?>
                    <div class="form-group">
                      <label for="qty">Sub Total</label>
                      <input type="number" name="subtotal" value="0" id="subtotal_transaksi" class="form-control text-dark" disabled>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <?= form_open(); ?>
                    <div class="form-group">
                      <label for="qty">Charge</label>
                      <input type="number" name="charge" value="0" id="charge_transaksi" class="form-control text-dark" disabled>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-3 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <?= form_open(); ?>
                      <div class="form-group">
                        <label for="qty">Note</label>
                        <textarea type="text" name="note_transaksi" id="note_transaksi" class="form-control text-white"></textarea>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-3 grid-margin">
                <div class="card">
                  <div class="card-body">
                      <button type="reset" class="btn btn-danger col-md-12 mt-3">Batalkan Transaksi</button>
                      <button type="button" id="gas" class="btn btn-primary col-md-12 mt-3">Tambah Transaksi</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          
<script type="text/javascript">
var num = 0;
var total_trx = 0;
var mn = document.getElementById("menu_transaksi");
var data_transaksi = [];
function totalBtn(){
    // MODIFIKASI ELEMENT TABEL DARI TOMBOL MENU
    let id_transaksi = $("#id_trx").html();
    let waktu = document.getElementById('waktu_transaksi').value;
    let id_user = document.getElementById('id_user').value;
    let nama_menu = mn.options[mn.selectedIndex].text;
    let menu =  document.getElementById('menu_transaksi').value;
    let qty =  document.getElementById('qty_transaksi').value;
    let jml = document.getElementById('jml_trx').innerHTML = menu*qty;

    // MODIFIKASI KOLOM INPUT BAWAH 
    let note = document.getElementById('note_transaksi').value;
    let sub = document.getElementById('subtotal_transaksi').value = total_trx += jml;
    let charge = document.getElementById('charge_transaksi').value = sub * 10/100;

    // INCREMENTAL NOMOR TABEL
    num++;
    let no = document.getElementById('nom').innerHTML = num;

    // GENERATE MENU
    var myHtmlContent = "<td id='nom'>"+no+"</td><td id='id_trx'><?= $invoice ?></td><td id='usr_tx'><?= $this->session->userdata('id_user') ?></td><td id='menu_trx'>"+nama_menu+"</td><td id='qty_trx'>"+qty+"</td><td id='jml_trx'>"+jml+"</td>"
    var tableRef = document.getElementById('add-transaksi').getElementsByTagName('tbody')[0];
    var newRow = tableRef.insertRow(tableRef.rows.length);
    newRow.innerHTML = myHtmlContent;
    
    // TOTAL PENJUMLAHAN DI INVOICE DAN GRAND TOTAL
    document.getElementById('total_transaksi').value = sub;
    let id_menu = mn.options[mn.selectedIndex].id;

    let data_trx = [id_transaksi,id_user,id_menu,jml,qty,note,waktu];
    data_transaksi.push(data_trx);
    console.log(data_transaksi); 
}
</script>
        <!-- partial -->
        <div class="main-panel" id="printableArea">
          <div class="content-wrapper">
            <div class="page-header mt-5 pt-2 d-print-none">
              <h3 class="page-title"> Detail Transaksi</h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Data Cafe</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Transaksi</li>
                </ol>
              </nav>
            </div>
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <h4 class="card-title">Detail Data Transaksi Invoice#<?= print_r($id["id_transaksi"]) ?></h4>
                        <p class="card-description"> Struk Transaksi Kasir Cafe Talis Invoice#<?= print_r($id['id_transaksi']) ?></p>
                      </div>
                    </div>
                    <div class="table-responsive">
                      <table class="table text-white py-4">
                        <thead>
                          <tr>
                            <th>No.</th>
                            <th>Id Transaksi</th>
                            <th>Id User</th>
                            <th>Nama Menu</th>
                            <th>Quantity</th>
                            <th>Jumlah Pemesanan</th>
                            <th>Waktu Transaksi</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $no = 1;
                        foreach ($transaksi as $j) :
                        ?>
                            <tr>
                              <td><?=$no++?></td>
                              <td>Invoice#<?=$j['id_transaksi']?></td>
                              <td><?=$j['id_user']?></td>
                              <td><?=$j['id_menu']?></td>
                              <td class='qty_trx'><?=$j['qty']?></td>
                              <td class='jml_trx'><?=$j['jumlah']?></td>
                              <td><?=$j['waktu_transaksi']?></td>
                              <td>
                              </td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-6 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <?= form_open(); ?>
                    <div class="form-group">
                      <label for="subtotal">Sub Total</label>
                      <input type="number" name="subtotal" value="0" id="subtotal_transaksi" class="form-control text-dark" disabled>
                    </div>
                    <div class="form-group">
                      <label for="discount">Discount</label>
                      <input type="number" name="discount" placeholder="0" id="discount_transaksi" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="qty">Total Quantity</label>
                      <input type="number" name="qty" value="0" id="qty_transaksi" class="form-control text-dark" disabled>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-6 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <div class="form-group">
                      <label for="cash">Cash</label>
                      <input type="number" name="cash" placeholder="0" id="cash_transaksi" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="charge">Charge</label>
                      <input type="number" name="charge" value="0" id="charge_transaksi" class="form-control text-dark" disabled>
                    </div>
                    <div class="form-group">
                      <label for="grand">Grand Total</label>
                      <input type="number" name="grand" value="0" id="grand_transaksi" class="form-control text-dark" disabled>
                    </div>
                    <div class="form-group">
                      <label for="change">Change</label>
                      <input type="number" name="change" value="0" id="change_transaksi" class="form-control text-dark" disabled>
                    </div>
                    <div class="btn-group btn-block d-print-none">
                      <button type="button" onclick="confirmBtn()" class="btn btn-outline-success"><i class="mdi mdi-check"></i> Konfirmasi</button>
                      <button onclick="printDiv('printableArea')" class="btn btn-outline-info"><i class="mdi mdi-printer"></i>Print</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
          </div>
        </div>
<script type="text/javascript">
  function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     window.print();
     document.body.innerHTML = originalContents;
  }
</script>
<script type="text/javascript">
    var jm = document.getElementsByClassName('jml_trx');
    for(var i = 0; i < jm.length; i++) {
        var j = jm[i].innerHTML;
    }
    var jml = parseInt(j)*jm.length;

    function confirmBtn(){
      let sub = document.getElementById('subtotal_transaksi').value = jml;
      let cash = document.getElementById('cash_transaksi').value;
      let disc = document.getElementById('discount_transaksi').value;
      let charge = document.getElementById('charge_transaksi').value = sub * 10/100;
      let grand = document.getElementById('grand_transaksi').value = sub + charge;
      let change = document.getElementById('change_transaksi').value = cash - grand - disc;
      document.getElementById('change_transaksi').value = grand - cash - disc;
    }
</script>
<script type="text/javascript">
    var num = 0;
    var total_trx = 0;
    var mn = document.getElementById("menu_transaksi");

    var qt = document.getElementsByClassName('qty_trx');
    for(var i = 0; i < qt.length; i++) {
        var q = qt[i].innerHTML;
    }
    var qty = parseInt(q)*qt.length;

    var jm = document.getElementsByClassName('jml_trx');
    for(var i = 0; i < jm.length; i++) {
        var j = jm[i].innerHTML;
    }
    var jml = parseInt(j)*jm.length;

    document.addEventListener("DOMContentLoaded", function(event) {
        // MODIFIKASI ELEMENT TABEL DARI TOMBOL MENU
        let sub = document.getElementById('subtotal_transaksi').value = jml;
        let charge = document.getElementById('charge_transaksi').value = sub * 10/100;
        let grand = document.getElementById('grand_transaksi').value = sub + charge;
        
        // TOTAL PENJUMLAHAN DI INVOICE DAN GRAND TOTAL
        document.getElementById('charge_transaksi').value = sub * 10/100;
        document.getElementById('qty_transaksi').value = qty;
        document.getElementById('subtotal_transaksi').value = sub;
        document.getElementById('grand_transaksi').value = sub + charge;
    });
</script>